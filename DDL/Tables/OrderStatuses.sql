use TrainingDB
go

if OBJECT_ID('dbo.OrderStatuses', 'U') is not null
    drop table dbo.OrderStatuses
go

create table dbo.OrderStatuses
(
    OrderStatusId int          not null identity,
    Name          nvarchar(20) not null,

    constraint PK_OrderStatuses primary key (OrderStatusId)
);
go