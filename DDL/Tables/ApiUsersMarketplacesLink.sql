use TrainingDB go

if OBJECT_ID('dbo.ApiUsersMarketplacesLink', 'U') is not null
drop table dbo.ApiUsersMarketplacesLink
    go

create table dbo.ApiUsersMarketplacesLink
(
    MarketplaceId int not null,
    ApiUserId     int not null,

    constraint PK_ApiUser_Marketplaces primary key (MarketplaceId, ApiUserId)
);
go