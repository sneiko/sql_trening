use TrainingDB
go

if OBJECT_ID('dbo.Reviewers', 'U') is not null
    drop table dbo.Reviewers
go

create table dbo.Reviewers
(
    ReviewId    int          not null identity,
    Rating      int          not null,
    Description nvarchar(80) not null,
    ReadById    int,
    UserId      int          not null,
    CreationAt  datetime     not null default CURRENT_TIMESTAMP,
    OrderId     int          not null,

    constraint PK_Reviewers primary key (ReviewId)
);
go