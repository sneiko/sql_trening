use TrainingDB
go

if OBJECT_ID('dbo.Marketplaces', 'U') is not null
    drop table dbo.Marketplaces
go

create table dbo.Marketplaces
(
    MarketplaceId    int          not null identity,
    Name             nvarchar(20) not null,
    Description      nvarchar(80) not null,
    StatusId         int          not null,
    CreationDateTime datetime              default CURRENT_TIMESTAMP,
    IsEnable         int          not null default 1,

    constraint PK_Marketplaces primary key (MarketplaceId)
);
go