use TrainingDB
go

if OBJECT_ID('dbo.UserType', 'U') is not null
    drop table dbo.UserType
go

create table dbo.UserType
(
    UserTypeId int      not null identity,
    Name       nvarchar(20) not null,

    constraint PK_UserType primary key (UserTypeId)
);
go