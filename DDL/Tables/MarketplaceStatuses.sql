use TrainingDB
go

if OBJECT_ID('dbo.MarketplaceStatuses', 'U') is not null
    drop table dbo.MarketplaceStatuses
go

create table dbo.MarketplaceStatuses
(
    MarketplaceStatusId int          not null identity,
    Name                nvarchar(20) not null,

    constraint PK_MarketplaceStatus primary key (MarketplaceStatusId)
);
go