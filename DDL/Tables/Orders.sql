use TrainingDB
go

if OBJECT_ID('dbo.Orders', 'U') is not null
    drop table dbo.Orders
go

create table dbo.Orders
(
    OrderId          int          not null identity,
    DeliveryAddress  nvarchar(60) not null,
    OrderSum         float        not null,
    ClientId         int          not null,
    DeliveryUserId   int,
    OperatorUserId   int,
    CookUserId       int,
    CookTime         int          not null,
    DeliveryTime     int,
    OrderStatusId    int          not null,
    MarketplaceId    int,
    CreationDateTime datetime default CURRENT_TIMESTAMP,

    constraint PK_Orders primary key (OrderId)
);
go