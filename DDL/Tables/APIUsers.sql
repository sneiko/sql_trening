use TrainingDB
go

if OBJECT_ID('dbo.APIUsers', 'U') is not null
    drop table dbo.APIUsers
go

create table dbo.APIUsers
(
    APIUserId            int          not null identity,
    Name                 nvarchar(30) not null,
    Login                nvarchar(20) not null,
    HashPassword         char(64)     not null,
    PhoneNumber          nvarchar(20) not null,
    DateTimeLastAuth     datetime,
    DateTimeRegistration datetime default CURRENT_TIMESTAMP,

    constraint PK_APIUser primary key (APIUserId)
);
go
