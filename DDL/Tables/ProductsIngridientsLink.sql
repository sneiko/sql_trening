use TrainingDB
go

if OBJECT_ID('dbo.ProductsIngredientsLink', 'U') is not null
    drop table dbo.ProductsIngredientsLink
go

create table dbo.ProductsIngredientsLink
(
    ProductId         int      not null,
    IngredientId      int      not null,
    Quantity          nvarchar(20) not null,
    FinalPricePerUnit float        not null,
    FinalPrice        float        not null,

    constraint PK_ProductsIngredientsLink primary key (IngredientId, ProductId)
);
go