use TrainingDB
go

if OBJECT_ID('dbo.Clients', 'U') is not null
    drop table dbo.Clients
go

create table dbo.Clients
(
    ClientId    int          not null identity,
    Name        nvarchar(30) not null,
    PhoneNumber nvarchar(20) not null,

    constraint PK_Clients primary key (ClientId)
);
go
