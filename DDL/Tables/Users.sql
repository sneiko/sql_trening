use TrainingDB
go

if OBJECT_ID('dbo.Users', 'U') is not null
    drop table dbo.Users
go

create table dbo.Users
(
    UserId               int          not null identity,
    Name                 nvarchar(30) not null,
    Login                nvarchar(30) not null,
    HashPassword         char(64)     not null,
    PhoneNumber          nvarchar(20) not null,
    UserTypeId           int          not null,
    DateTimeLastAuth     datetime,
    DateTimeRegistration datetime              default CURRENT_TIMESTAMP,
    isEnabled            int          not null default 0,

    constraint PK_Users primary key (UserId)
);
go