use TrainingDB
go

if OBJECT_ID('dbo.Administrators', 'U') is not null
    drop table dbo.Administrators
go

create table dbo.Administrators
(
    AdministratorId  int          not null identity,
    Name             nvarchar(30) not null,
    Login            nvarchar(30) not null,
    HashPassword     char(64)     not null,
    PhoneNumber      nvarchar(20) not null,
    Email            nvarchar(30) not null,
    DateTimeLastAuth datetime,

    constraint PK_Administrators primary key (AdministratorId)
);
go