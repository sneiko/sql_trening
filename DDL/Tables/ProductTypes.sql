use TrainingDB
go

if OBJECT_ID('dbo.ProductTypes', 'U') is not null
    drop table dbo.ProductTypes
go

create table dbo.ProductTypes
(
    ProductTypeId int      not null identity,
    Name   nvarchar(20) not null,

    constraint PK_ProductTypes primary key (ProductTypeId)
);
go