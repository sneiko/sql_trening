use TrainingDB
go

if OBJECT_ID('dbo.Ingredients', 'U') is not null
    drop table dbo.Ingredients
go

create table dbo.Ingredients
(
    IngredientId int          not null identity,
    Name         nvarchar(20) not null UNIQUE,
    Callories    int          not null,

    constraint PK_Ingredients primary key (IngredientId)
);
go