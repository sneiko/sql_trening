use TrainingDB
go

if OBJECT_ID('dbo.OrdersProductsLink', 'U') is not null
    drop table dbo.OrdersProductsLink
go

create table dbo.OrdersProductsLink
(
    OrderId   int          not null,
    ProductId int          not null,
    Quantity  nvarchar(20) not null,

    constraint PK_OrdersProductsLink primary key (OrderId, ProductId)
);
go