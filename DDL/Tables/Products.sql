use TrainingDB
go

if OBJECT_ID('dbo.Products', 'U') is not null
    drop table dbo.Products
go

create table dbo.Products
(
    ProductId        int      not null identity,
    Name             nvarchar(20) not null,
    Description      nvarchar(80) not null,
    ProductTypeId    int          not null,
    CookTime         int          not null,
    CreationDateTIme datetime default CURRENT_TIMESTAMP,
    AwailableForSale int          not null,

    constraint PK_Products primary key (ProductId)
);
go