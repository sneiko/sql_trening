use TrainingDB
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[ProductTypes]',
	@constraintName		= 'AK_ProductTypes_Name_Value',
	@fields				= 'Name'
go