use TrainingDB
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Administrators]',
	@constraintName		= 'AK_Administrators_Login_Value',
	@fields				= 'Login'
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Administrators]',
	@constraintName		= 'AK_Administrators_EMail_Value',
	@fields				= 'EMail'
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Administrators]',
	@constraintName		= 'AK_Administrators_PhoneNumber_Value',
	@fields				= 'PhoneNumber'
go