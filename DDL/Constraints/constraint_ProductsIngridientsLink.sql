use TrainingDB
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[ProductsIngredientsLink]',
    @targetFields    = 'ProductId',
    @parentTableName = 'dbo.[Products]',
    @parentFields    = 'ProductId',
    @constraintName  = 'FK_ProductsIngredientsLink_ProductId'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[ProductsIngredientsLink]',
    @targetFields    = 'IngredientId',
    @parentTableName = 'dbo.[Ingredients]',
    @parentFields    = 'IngredientId',
    @constraintName  = 'FK_ProductsIngredientsLink_IngredientId'
go