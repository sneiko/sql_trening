use TrainingDB
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[UserType]',
	@constraintName		= 'AK_UserTypes_Name_Value',
	@fields				= 'Name'
go