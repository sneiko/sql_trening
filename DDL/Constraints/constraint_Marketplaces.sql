use TrainingDB
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Marketplaces]',
	@constraintName		= 'AK_Marketplaces_Name_Value',
	@fields				= 'Name'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Marketplaces]',
    @targetFields    = 'StatusId',
    @parentTableName = 'dbo.[MarketplaceStatuses]',
    @parentFields    = 'MarketplaceStatusId',
    @constraintName  = 'FK_Marketplaces_MarketplaceStatus'
go