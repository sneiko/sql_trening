use TrainingDB
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Ingredients]',
	@constraintName		= 'AK_Ingredients_Name_Value',
	@fields				= 'Name'
go