use TrainingDB
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[OrdersProductsLink]',
    @targetFields    = 'OrderId',
    @parentTableName = 'dbo.[Orders]',
    @parentFields    = 'OrderId',
    @constraintName  = 'FK_OrdersProductsLink_OrderId'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[OrdersProductsLink]',
    @targetFields    = 'ProductId',
    @parentTableName = 'dbo.[Products]',
    @parentFields    = 'ProductId',
    @constraintName  = 'FK_OrdersProductsLink_ProductId'
go