use TrainingDB
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Products]',
    @targetFields    = 'ProductTypeId',
    @parentTableName = 'dbo.[ProductTypes]',
    @parentFields    = 'ProductTypeId',
    @constraintName  = 'FK_Products_ProductTypes'
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Products]',
	@constraintName		= 'AK_Products_Name_Value',
	@fields				= 'Name'
go