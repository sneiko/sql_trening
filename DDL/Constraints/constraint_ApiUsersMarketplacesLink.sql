use TrainingDB
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[ApiUsersMarketplacesLink]',
    @targetFields    = 'ApiUserId',
    @parentTableName = 'dbo.[ApiUsers]',
    @parentFields    = 'ApiUserId',
    @constraintName  = 'FK_ApiUsersMarketplacesLink_ApiUserId'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[ApiUsersMarketplacesLink]',
    @targetFields    = 'MarketplaceId',
    @parentTableName = 'dbo.[Marketplaces]',
    @parentFields    = 'MarketplaceId',
    @constraintName  = 'FK_ApiUsersMarketplacesLink_MarketplaceId'
go