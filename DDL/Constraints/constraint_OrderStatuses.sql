use TrainingDB
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[OrderStatuses]',
	@constraintName		= 'AK_OrderStatuses_Name_Value',
	@fields				= 'Name'
go