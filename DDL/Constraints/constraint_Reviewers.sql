use TrainingDB
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Reviewers]',
    @targetFields    = 'ReadById',
    @parentTableName = 'dbo.[Administrators]',
    @parentFields    = 'AdministratorId',
    @constraintName  = 'FK_Reviewers_AdministratorId'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Reviewers]',
    @targetFields    = 'UserId',
    @parentTableName = 'dbo.[Users]',
    @parentFields    = 'UserId',
    @constraintName  = 'FK_Reviewers_UserId'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Reviewers]',
    @targetFields    = 'OrderId',
    @parentTableName = 'dbo.[Orders]',
    @parentFields    = 'OrderId',
    @constraintName  = 'FK_Reviewers_OrderId'
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Reviewers]',
	@constraintName		= 'AK_Reviewers_CreationAt_Value',
	@fields				= 'CreationAt'
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Reviewers]',
	@constraintName		= 'AK_Product_UserId',
	@fields				= 'UserId'
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Reviewers]',
	@constraintName		= 'AK_Product_OrderId',
	@fields				= 'OrderId'
go