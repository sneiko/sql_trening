use TrainingDB
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Orders]',
    @targetFields    = 'ClientId',
    @parentTableName = 'dbo.[Clients]',
    @parentFields    = 'ClientId',
    @constraintName  = 'FK_Orders_Clients'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Orders]',
    @targetFields    = 'DeliveryUserId',
    @parentTableName = 'dbo.[Users]',
    @parentFields    = 'UserId',
    @constraintName  = 'FK_Orders_DeliveryUsers'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Orders]',
    @targetFields    = 'OperatorUserId',
    @parentTableName = 'dbo.[Users]',
    @parentFields    = 'UserId',
    @constraintName  = 'FK_Orders_OperatorUsers'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Orders]',
    @targetFields    = 'CookUserId',
    @parentTableName = 'dbo.[Users]',
    @parentFields    = 'UserId',
    @constraintName  = 'FK_Orders_CookUsers'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Orders]',
    @targetFields    = 'OrderStatusId',
    @parentTableName = 'dbo.[OrderStatuses]',
    @parentFields    = 'OrderStatusId',
    @constraintName  = 'FK_Order_OrderStatus'
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Orders]',
    @targetFields    = 'MarketplaceId',
    @parentTableName = 'dbo.[Marketplaces]',
    @parentFields    = 'MarketplaceId',
    @constraintName  = 'FK_Orders_Marketplaces'
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Orders]',
	@constraintName		= 'AK_DeliveryAddress_Value',
	@fields				= 'DeliveryAddress'
go