use TrainingDB
go

exec dbo.usp_AddForeignKey
    @targetTableName = 'dbo.[Users]',
    @targetFields    = 'UserTypeId',
    @parentTableName = 'dbo.[UserType]',
    @parentFields    = 'UserTypeId',
    @constraintName  = 'FK_User_UserType'
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Users]',
	@constraintName		= 'AK_User_Login_Value',
	@fields				= 'Login'
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Users]',
	@constraintName		= 'AK_User_PhoneNumber_Value',
	@fields				= 'PhoneNumber'
go