use TrainingDB
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[Clients]',
	@constraintName		= 'AK_Clients_PhoneNumber_Value',
	@fields				= 'PhoneNumber'
go