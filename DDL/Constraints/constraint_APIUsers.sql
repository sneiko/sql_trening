use TrainingDB
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[APIUsers]',
	@constraintName		= 'AK_APIUser_Login_Value',
	@fields				= 'Login'
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[APIUsers]',
	@constraintName		= 'AK_APIUser_PhoneNumber_Value',
	@fields				= 'PhoneNumber'
go