use TrainingDB
go

exec dbo.usp_AddAlternateKey
	@tableName			= 'dbo.[MarketplaceStatuses]',
	@constraintName		= 'AK_MarketplaceStatus_Name_Value',
	@fields				= 'Name'
go