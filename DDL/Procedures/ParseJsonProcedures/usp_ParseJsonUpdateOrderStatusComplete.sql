use TrainingDB
go

if object_id('dbo.usp_ParseJsonUpdateOrderStatusComplete', 'P') is not null
    drop procedure dbo.usp_ParseJsonUpdateOrderStatusComplete
go

create proc dbo.usp_ParseJsonUpdateOrderStatusComplete(
    @json nvarchar(max)
)
as
begin

    UPDATE Orders
    SET DeliveryUserId = du.UserId,
        DeliveryTime   = DATEDIFF(ms, DATEADD(ms, CookTime, CreationDateTime), GETDATE()),
        OrderStatusId  = os.OrderStatusId
    FROM openjson(@json)
                  with
                      (
                      d_uLogin nvarchar(30) '$.DeliveryUser.Login',
                      orderId int '$.Order.OrderId',
                      statusName nvarchar(50) '$.Order.Status'
                      ) as ps
             INNER JOIN Users du ON du.Login = ps.d_uLogin
             INNER JOIN OrderStatuses os ON os.Name = ps.statusName
    WHERE Orders.OrderId = ps.orderId
end
go