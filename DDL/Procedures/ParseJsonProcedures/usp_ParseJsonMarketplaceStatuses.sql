use TrainingDB
go

if object_id('dbo.usp_ParseJsonAddUserType', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddUserType
go

create proc dbo.usp_ParseJsonAddUserType
(
    @json nvarchar(max)
)
as
begin
	insert into dbo.UserType
	(
		Name
	)
	select Json_Name
	from openjson(@json)
	with
		(
			Json_Name varchar(20) '$.Name'
		) as S
end
go