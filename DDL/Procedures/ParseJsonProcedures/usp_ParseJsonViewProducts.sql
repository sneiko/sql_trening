use TrainingDB
go

if object_id('dbo.usp_ParseJsonViewProducts', 'P') is not null
    drop procedure dbo.usp_ParseJsonViewProducts
go

create proc dbo.usp_ParseJsonViewProducts(
    @json nvarchar(max)
)
as
begin
    begin try
        begin transaction
            create table #jsonData
            (
                CreationDateTime datetime,
                AwailableForSale int
            );

            with jsonData (jsonCreationDateTime, awailableForSale) as
                     (
                         select js.jsonCreationDateTime,
                                js.awailableForSale
                         from OPENJSON(@Json) with
                             (
                             jsonCreationDateTime datetime '$.Product.CreationDateTIme',
                             awailableForSale int '$.Product.AwailableForSale'
                             ) js
                     )
            INSERT
            INTO #jsonData (CreationDateTime, AwailableForSale)
            SELECT jsonData.jsonCreationDateTime, jsonData.awailableForSale
            FROM jsonData

            declare @CreationDateTimeFilterValue datetime
            select @CreationDateTimeFilterValue = #jsonData.CreationDateTime from #jsonData

            declare @AwailableForSaleTimeFilterValue int
            select @AwailableForSaleTimeFilterValue = #jsonData.AwailableForSale from #jsonData

            if (@CreationDateTimeFilterValue IS NULL)
                begin
                    print '[ERROR] CreationDateTime IS NULL'
                    rollback
                    return
                end

            if (@AwailableForSaleTimeFilterValue IS NULL)
                begin
                    print '[ERROR] AwailableForSale IS NULL'
                    rollback
                    return
                end

            if (@CreationDateTimeFilterValue IS NOT NULL AND @AwailableForSaleTimeFilterValue IS NOT NULL)
                begin
                    declare @ResultQuery nvarchar(max)
                    set @ResultQuery = N'select p.Name as Name,
                                       pt.Name as ProductType,
                                       p.AwailableForSale as AwailableForSale,
                                       pil.FinalPrice as Price,
                                       p.CookTime as CookTime
                                from Products as p
                                    INNER JOIN ProductsIngredientsLink pil ON pil.ProductId = p.ProductId
                                    INNER JOIN ProductTypes pt ON pt.ProductTypeId = p.ProductTypeId

                                where CreationDateTIme > CAST(''{creationDateTime}'' as datetime) AND
                                      AwailableForSale = {awailabelForSale}'

                    set @ResultQuery = replace(@ResultQuery, N'{creationDateTime}', @CreationDateTimeFilterValue)
                    set @ResultQuery = replace(@ResultQuery, N'{awailabelForSale}', @AwailableForSaleTimeFilterValue)
                    exec (@ResultQuery)
                end
        commit

    end try
    begin catch
        select ERROR_NUMBER()  AS [error number],
               ERROR_MESSAGE() AS [error message]
        rollback
    end catch
end
go