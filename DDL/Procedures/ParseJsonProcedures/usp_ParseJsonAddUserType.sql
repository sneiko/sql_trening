use TrainingDB
go

if object_id('dbo.usp_ParseJsonMarketplaceStatuses', 'P') is not null
    drop procedure dbo.usp_ParseJsonMarketplaceStatuses
go

create proc dbo.usp_ParseJsonMarketplaceStatuses
(
    @json nvarchar(max)
)
as
begin
	insert into dbo.MarketplaceStatuses
	(
		Name
	)
	select Json_Name
	from openjson(@json)
	with
		(
			Json_Name varchar(20) '$.Name'
		) as S
end
go