use TrainingDB
go

if object_id('dbo.usp_ParseJsonReadReview', 'P') is not null
    drop procedure dbo.usp_ParseJsonReadReview
go

create proc dbo.usp_ParseJsonReadReview(
    @json nvarchar(max)
)
as
begin

    SELECT a.Name as ReadBy,
           r.Rating,
           r.Description,
           r.CreationAt
    FROM openjson(@json)
                  with
                      (
                      rID int '$.Review.Id',
                      aLogin nvarchar(30) '$.Administrator.Login'
                      ) as ps
             INNER JOIN Administrators a ON a.Login = ps.aLogin
             INNER JOIN Reviewers r on rID = r.ReviewId
    WHERE ReviewId = ps.rID

end
go