use TrainingDB
go

if object_id('dbo.usp_ParseJsonAddReviewers', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddReviewers
go

CREATE PROC dbo.usp_ParseJsonAddReviewers(
    @json nvarchar(max)
)
AS
BEGIN
    INSERT INTO dbo.Reviewers
        (Rating, Description, UserId, OrderId)
    SELECT json.Rating,
           json.Description,
           U.UserId,
           O.OrderId
    FROM openjson(@json)
                  WITH
                      (
                      Rating int '$.Review.Rating',
                      Description nvarchar(80) '$.Review.Description',
                      UserLogin varchar(50) '$.User.Login',
                      ClientPhoneNumber varchar(50) '$.Client.PhoneNumber',
                      OrderDeliveryAdress varchar(50) '$.Order.DeliveryAdress',
                      OrderCreateDatetime datetime '$.Order.DateTimeCreation'
                      ) AS json
             LEFT JOIN Users AS U ON U.Login = json.UserLogin
             LEFT JOIN Clients C on json.ClientPhoneNumber = C.PhoneNumber
             LEFT JOIN Orders O
                       ON C.ClientId = O.ClientId AND
                          O.DeliveryAddress = json.OrderDeliveryAdress AND
                          O.CreationDateTime = json.OrderCreateDatetime


END
go