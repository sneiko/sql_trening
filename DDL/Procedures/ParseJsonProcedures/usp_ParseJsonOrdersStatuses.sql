use TrainingDB
go

if object_id('dbo.usp_ParseJsonOrdersStatuses', 'P') is not null
    drop procedure dbo.usp_ParseJsonOrdersStatuses
go

create proc dbo.usp_ParseJsonOrdersStatuses
(
    @json nvarchar(max)
)
as
begin
	insert into dbo.OrderStatuses
	(
		Name
	)
	select Json_Name
	from openjson(@json)
	with
		(
			Json_Name varchar(20) '$.Name'
		) as S
end
go