use TrainingDB
go

if object_id('dbo.usp_ParseJsonAddProductType', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddProductType
go

create proc dbo.usp_ParseJsonAddProductType
(
    @json nvarchar(max)
)
as
begin
	insert into dbo.ProductTypes
	(
		Name
	)
	select Json_Name
	from openjson(@json)
	with
		(
			Json_Name varchar(20) '$.Name'
		) as S
end
go