use TrainingDB
go

if object_id('dbo.usp_ParseJsonAddProduct', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddProduct
go

create proc dbo.usp_ParseJsonAddProduct(
    @json nvarchar(max)
)
as

begin
    begin transaction;

    create table #Ingredients
    (
        Name         nvarchar(20) UNIQUE,
        Callories    int,
        Quantity     int,
        PricePerUnit float,
    );

    INSERT INTO Products
        (Name, Description, ProductTypeId, CookTime, AwailableForSale)
    SELECT json_Name,
           json_Description,
           pt.ProductTypeId,
           json_CookTime,
           json_AwailableForSale
    from openjson(@json) with (
        json_Name varchar(20) '$.Product.Name',
        json_Description varchar(20) '$.Product.Description',
        json_CookTime int '$.Product.CookTime',
        json_ProductTypeName varchar(20) '$.Product.ProductType.Name',
        json_AwailableForSale int '$.Product.AwailableForSale'
        )
             left join ProductTypes as pt
                       on pt.Name = json_ProductTypeName


    INSERT
    INTO #Ingredients
        (Name, Callories, Quantity, PricePerUnit)
    SELECT json_i_Name,
           json_i_Callories,
           json_i_Quantity,
           json_i_PricePerUnit
    FROM
        openjson(@json, '$.Ingredient')
                 WITH
                     (
                     json_i_Name nvarchar(30) '$.Name',
                     json_i_Callories int '$.Callories',
                     json_i_Quantity int '$.Quantity',
                     json_i_PricePerUnit float '$.PricePerUnit'
                     );
    select * from #Ingredients

--     ADD NEW INGREDIENTS
    INSERT INTO Ingredients (Name, Callories)
    SELECT #i.Name, #i.Callories
    FROM Ingredients as i
             RIGHT JOIN #Ingredients as #i
                        ON i.Name = #i.Name
    WHERE i.Name is null


--     Add Link with Product and Ingridients
    INSERT INTO ProductsIngredientsLink
        (ProductId, IngredientId, Quantity, FinalPrice, FinalPricePerUnit)
    SELECT p.ProductId,
           i.IngredientId,
           #i.Quantity,
           (#i.PricePerUnit * #i.Quantity) as FinalPrice,
           #i.PricePerUnit
    FROM Ingredients as i
             LEFT JOIN #Ingredients as #i
                       ON i.Name = #i.Name
             LEFT JOIN Products as p
                       ON p.Name = (
                           select json_Name
                           from openjson(@json) with
                               (
                               json_Name varchar(20) '$.Product.Name'
                               )
                       ) where p.ProductTypeId is not null and
                               #i.Quantity is not null
    commit
end

go