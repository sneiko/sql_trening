use TrainingDB
go

if object_id('dbo.usp_ParseJsonViewAnalytics', 'P') is not null
    drop procedure dbo.usp_ParseJsonViewAnalytics
go

create proc dbo.usp_ParseJsonViewAnalytics(
    @json nvarchar(max)
)
as
begin
    begin try
        begin transaction

            -- PARSE JSON
            DECLARE @ShowNullRows bit;
            SET @ShowNullRows = (SELECT ShowNullRows
                                 FROM openjson(@json) with (
                                     ShowNullRows bit '$.ShowNullRows'
                                     ))
            DECLARE @ShowNullCols bit;
            SET @ShowNullCols = (SELECT ShowNullCols
                                 FROM openjson(@json) with (
                                     ShowNullCols bit '$.ShowNullCols'
                                     ))

            create table #TempClients
            (
                ClientId    int,
                PhoneNumber varchar(30),
            )

            create table #TempProducts
            (
                ProductId int,
                Name      varchar(800),
            )

            insert into #TempClients(ClientId, PhoneNumber)
            select ClientId, dbClient.PhoneNumber
            from Clients as dbClient
                     inner join (select PhoneNumber
                                 from openjson(@json, '$.Clients') with (
                                     PhoneNumber nvarchar(30) '$.PhoneNumber'
                                     )) as jClient on dbClient.PhoneNumber = jClient.PhoneNumber

            if (SELECT COUNT(*) FROM #TempClients) = 0
                begin
                    insert into #TempClients(ClientId, PhoneNumber)
                    select c.ClientId, c.PhoneNumber
                    from Clients as c
                end


            insert into #TempProducts(ProductId, Name)
            select dbProduct.ProductId, dbProduct.Name
            from Products as dbProduct
                     inner join (select Name
                                 from openjson(@json, '$.Products') with (
                                     Name nvarchar(80) '$.Name'
                                     )) as jProduct on dbProduct.Name = jProduct.Name

            if (SELECT COUNT(*) FROM #TempProducts) = 0
                begin
                    insert into #TempProducts(ProductId, Name)
                    select p.ProductId, p.Name
                    from Products as p
                end

-- FILTERING
            create table #TempSelectTable
            (
                PhoneNumber varchar(30),
                ProductName varchar(80)
            )

            if @ShowNullRows = 1
                begin
                    INSERT INTO #TempSelectTable(PhoneNumber, ProductName)
                    SELECT #TempClients.PhoneNumber,
                           P.Name
                    from #TempClients
                             left outer join (Orders o
                        inner join OrdersProductsLink opl on opl.OrderId = o.OrderId
                        inner join #TempProducts P on opl.ProductId = P.ProductId
                        ) on o.ClientId = #TempClients.ClientId
                end
            else
                begin
                    INSERT INTO #TempSelectTable(PhoneNumber, ProductName)
                    SELECT #TempClients.PhoneNumber,
                           P.Name
                    from #TempClients
                             inner join (Orders o
                        inner join OrdersProductsLink opl on opl.OrderId = o.OrderId
                        inner join #TempProducts P on opl.ProductId = P.ProductId
                        ) on o.ClientId = #TempClients.ClientId
                end

            DECLARE @ProductList varchar(max)

            if @ShowNullCols = 1
                begin
                    SELECT @ProductList = COALESCE(@ProductList + ',', '') + '[' + ts.Name + ']'
                    FROM #TempProducts ts
                    GROUP BY ts.Name;
                end
            else
                begin
                    SELECT @ProductList = COALESCE(@ProductList + ',', '') + '[' + ts.ProductName + ']'
                    FROM #TempSelectTable as ts
                    GROUP BY ts.ProductName;
                end


-- OUTPUT

            DECLARE @sqlToRun varchar(max)
            SET @sqlToRun = 'SELECT PhoneNumber, ' + @ProductList + '
                                FROM (
                                         SELECT PhoneNumber, ProductName
                                         FROM #TempSelectTable
                                     ) as src
                                         PIVOT
                                         (
                                         COUNT(ProductName)
                                         FOR ProductName
                                         IN (' + @ProductList + ')
                                         ) as pvt'
            print (@sqlToRun)
            exec (@sqlToRun)
        commit

    end try
    begin catch
        select ERROR_NUMBER()  AS [error number],
               ERROR_MESSAGE() AS [error message]
        rollback
    end catch
end
go