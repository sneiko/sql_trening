use TrainingDB
go

if object_id('dbo.usp_ParseJsonMarketplace', 'P') is not null
    drop procedure dbo.usp_ParseJsonMarketplace
go

CREATE PROC dbo.usp_ParseJsonMarketplace(
    @json nvarchar(max)
)
AS
BEGIN
    begin transaction;

    INSERT INTO dbo.Marketplaces
        (Name, Description, StatusId)
    SELECT js_Name,
           js_Description,
           (select MarketplaceStatusId from MarketplaceStatuses WHERE Name = js_StatusName)
    FROM openjson(@json, '$.Marketplaces')
                  WITH
                      (
                      js_Name nvarchar(30) '$.Name',
                      js_Description nvarchar(30) '$.Description',
                      js_StatusName nvarchar(64) '$.StatusName'
                      ) as s


    INSERT INTO dbo.ApiUsersMarketplacesLink
        (MarketplaceId, ApiUserId)
    SELECT m.MarketplaceId,
           u.APIUserId
    FROM openjson(@json)
                  WITH
                      (
                      js_MarketplaceName nvarchar(30) '$.Marketplace.Name',
                      js_ApiUserLogin nvarchar(30) '$.APIUser.Login'
                      )
             inner join Marketplaces as m
                       on Name = js_MarketplaceName
             inner join APIUsers as u
                       on Login = js_ApiUserLogin
    commit
END
go