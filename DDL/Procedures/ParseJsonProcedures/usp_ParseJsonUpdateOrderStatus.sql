use TrainingDB
go

if object_id('dbo.usp_ParseJsonUpdateOrderStatus', 'P') is not null
    drop procedure dbo.usp_ParseJsonUpdateOrderStatus
go

create proc dbo.usp_ParseJsonUpdateOrderStatus(
    @json nvarchar(max)
)
as
begin

--     COOK
    if (select *
        from openjson(@json) with
            (
            d_uLogin nvarchar(30) '$.CookUser.Login'
            )
    ) is not null
        begin
            UPDATE Orders
            SET CookUserId = cu.UserId,
                CookTime   = DATEDIFF(ms, CreationDateTime, GETDATE()),
                OrderStatusId  = os.OrderStatusId
            FROM openjson(@json)
                          with
                              (
                              c_uLogin nvarchar(30) '$.CookUser.Login',
                              c_orderId int '$.Order.OrderId',
                              statusName nvarchar(50) '$.Order.Status'
                              ) as ps
                     LEFT JOIN Users cu ON cu.Login = ps.c_uLogin
                     LEFT JOIN OrderStatuses os ON os.Name = ps.statusName
            WHERE Orders.OrderId  = c_orderId
        end

-- --     DELIVERY
    if (select *
        from openjson(@json) with
            (
            d_uLogin nvarchar(30) '$.DeliveryUser.Login'
            )
    ) is not null
        begin
            UPDATE Orders
            SET DeliveryUserId = du.UserId,
                DeliveryTime   = DATEDIFF(d, DATEDIFF(d, 0, GETDATE()),
                                             DATEADD(ms, CookTime, CreationDateTime)),
                OrderStatusId  = os.OrderStatusId
            FROM openjson(@json)
                          with
                              (
                              d_uLogin nvarchar(30) '$.DeliveryUser.Login',
                              orderId int '$.Order.OrderId',
                              statusName nvarchar(50) '$.Order.Status'
                              ) as ps
                     LEFT JOIN Users du ON du.Login = ps.d_uLogin
                     LEFT JOIN OrderStatuses os ON os.Name = ps.statusName
            WHERE Orders.OrderId = ps.orderId
        end
end
go