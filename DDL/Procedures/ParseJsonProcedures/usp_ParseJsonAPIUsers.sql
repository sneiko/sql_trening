use TrainingDB
go

if object_id('dbo.usp_ParseJsonAPIUsers', 'P') is not null
    drop procedure dbo.usp_ParseJsonAPIUsers
go

CREATE PROC dbo.usp_ParseJsonAPIUsers(
    @json nvarchar(max)
)
AS
BEGIN
    INSERT INTO dbo.APIUsers
        (Name, Login, HashPassword, PhoneNumber, DateTimeLastAuth)
    SELECT js_Name,
           js_Login,
           js_HashPassword,
           js_PhoneNumber,
           js_DateTimeLastAuth
    FROM openjson(@json)
                  WITH
                      (
                      js_Name nvarchar(30) '$.Name',
                      js_Login nvarchar(30) '$.Login',
                      js_HashPassword char(64) '$.HashPassword',
                      js_PhoneNumber nvarchar(20) '$.PhoneNumber',
                      js_DateTimeLastAuth datetime '$.DateTimeLastAuth'
                      ) AS s
END
go