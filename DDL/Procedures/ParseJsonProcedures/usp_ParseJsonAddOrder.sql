use TrainingDB
go

if object_id('dbo.usp_ParseJsonAddOrder', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddOrder
go

CREATE PROC dbo.usp_ParseJsonAddOrder(
    @json nvarchar(max)
)
AS

DECLARE @OrderSum float
BEGIN

    --     Parse products and find ID in DB
    create table #Products
    (
        ProductId int,
        Name      varchar(50),
        Quantity  int
    )

    INSERT INTO #Products (ProductId, Name, Quantity)
    SELECT p.ProductId, j_p_Name, j_p_Quantity
    from openjson(@json, '$.Products') with
        (
        j_p_Name nvarchar(50) '$.Name',
        j_p_Quantity nvarchar(50) '$.ProductQuantity'
        )
             left join Products as p on p.Name = j_p_Name


--     Check CLIENT if absent insert new
    IF (
        select ROW_NUMBER() OVER (ORDER BY PhoneNumber)
        from dbo.Clients
        WHERE PhoneNumber = (
            select j_c_phoneNumber
            from openjson(@json)
                          WITH (
                              j_c_phoneNumber varchar(30) '$.Client.PhoneNumber'
                              )
        )) IS NULL
        BEGIN
            INSERT INTO dbo.Clients(Name, PhoneNumber)
            SELECT j_c_name, j_c_phoneNumber
            FROM openjson(@json)
                          WITH (
                              j_c_name varchar(30) '$.Client.Name',
                              j_c_phoneNumber varchar(30) '$.Client.PhoneNumber'
                              )
        END

    SET @OrderSum = (select SUM(qf.Quantity * qf.FinalPrice)
                     from (
                              select pi.Quantity, pi.FinalPrice
                              from #Products as #p
                                       left join dbo.Products p on p.Name = #p.Name
                                       left join ProductsIngredientsLink pi on pi.ProductId = p.ProductId
                          ) as qf)

    DECLARE @OrderIdTempTable TABLE (OrderId int)

    INSERT INTO Orders
    (DeliveryAddress, OrderSum, ClientId, OperatorUserId, CookTime, OrderStatusId, MarketplaceId)
    OUTPUT inserted.OrderId INTO @OrderIdTempTable
    SELECT j_DeliveryAdress,
           @OrderSum,
           C.ClientId,
           U.UserId,
           10,
           (SELECT OrderStatusId FROM OrderStatuses WHERE Name = 'Accept'),
           M.MarketplaceId
    FROM openjson(@json) WITH
        (
        j_DeliveryAdress varchar(80) '$.Order.DeliveryAdress',
        j_ClientPhone varchar(40) '$.Client.PhoneNumber',
        j_OperatorLogin varchar(80) '$.OperatorUser.Login',
        j_MarketplaceName varchar(80) '$.Marketplace.Name'
        )
             left join Clients as C on C.PhoneNumber = j_ClientPhone
             left join Users U on U.Login = j_OperatorLogin
             left join Marketplaces M on M.Name = j_MarketplaceName

    INSERT INTO OrdersProductsLink
        (OrderId, ProductId, Quantity)
    SELECT OrderIdTemp.OrderId, #p.ProductId, #p.Quantity
    FROM @OrderIdTempTable as OrderIdTemp
    OUTER APPLY #Products as #p
END
go

