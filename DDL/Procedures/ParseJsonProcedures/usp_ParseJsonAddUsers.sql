use TrainingDB
go

if object_id('dbo.usp_ParseJsonAddUsers', 'P') is not null
    drop procedure dbo.usp_ParseJsonAddUsers
go

CREATE PROC dbo.usp_ParseJsonAddUsers(
    @json nvarchar(max)
)
AS
BEGIN
    INSERT INTO dbo.Users
        (Name, Login, HashPassword, PhoneNumber, UserTypeId, DateTimeLastAuth)
    SELECT js_Name,
           js_Login,
           js_HashPassword,
           js_PhoneNumber,
           (
               SELECT UserTypeId
               FROM dbo.UserType
               WHERE Name = js_UserTypeName
           ),
           js_DateTimeLastAuth
    FROM openjson(@json)
                  WITH
                      (
                      js_Name nvarchar(30) '$.Name',
                      js_Login nvarchar(30) '$.Login',
                      js_HashPassword char(64) '$.HashPassword',
                      js_PhoneNumber nvarchar(20) '$.PhoneNumber',
                      js_UserTypeName nvarchar(30) '$.UserType.Name',
                      js_DateTimeLastAuth datetime '$.DateTimeLastAuth'
                      ) AS s
END
go