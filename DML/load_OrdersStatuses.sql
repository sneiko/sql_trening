use TrainingDB
go

exec dbo.usp_ParseJsonOrdersStatuses
    @json =N'{
        "Name": "Accept"
      }'
go

exec dbo.usp_ParseJsonOrdersStatuses
    @json =N'{
        "Name": "Cook"
      }'
go

exec dbo.usp_ParseJsonOrdersStatuses
    @json =N'{
        "Name": "InDelivery"
      }'
go

exec dbo.usp_ParseJsonOrdersStatuses
    @json =N'{
        "Name": "Complete"
      }'
go

exec dbo.usp_ParseJsonOrdersStatuses
    @json =N'{
        "Name": "ClientIsAbsent"
      }'
go