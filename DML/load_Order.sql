use TrainingDB
go

exec dbo.usp_ParseJsonAddOrder
     @json =N'{
        "Order": {
            "DeliveryAdress": "г.Минск, ул. Коммунистическая 111, кв 1",
            "DateTimeCreation": "04.02.2022T23:33:12"
        },
        "Client": {
            "Name": "ClientName-1",
            "PhoneNumber": "+375333252011"
        },
        "OperatorUser": {
            "Login": "sergey_operator"
        },
        "Marketplace": {
            "Name": null
        },
        "Products": [
            {
                "Name": "Coca-Cola 1l",
                "ProductQuantity": 1
            },
            {
                "Name": "Souse Teriaki",
                "ProductQuantity": 2
            }
        ]
    }'
go