use TrainingDB
go

exec dbo.usp_ParseJsonAddUsers
    @json ='{
    "Name": "Sergey Operator",
    "Login": "sergey_operator",
    "HashPassword": "81dc9bdb52d04dc20036dbd8313ed055",
    "PhoneNumber": "+375333252222",
    "UserType": {
      "Name": "Operator"
    },
    "DateTimeLastAuth": "2021-05-17T00:00:00.000"
  }'
go

exec dbo.usp_ParseJsonAddUsers
    @json ='{
    "Name": "Sergey Cook",
    "Login": "sergey_cook",
    "HashPassword": "81dc9bdb52d04dc20036dbd8313ed055",
    "PhoneNumber": "+375333252223",
    "UserType": {
      "Name": "Cook"
    },
    "DateTimeLastAuth": "2021-05-17T00:00:00.000"
  }'
go

exec dbo.usp_ParseJsonAddUsers
    @json ='{
    "Name": "Sergey Driver",
    "Login": "sergey_driver",
    "HashPassword": "81dc9bdb52d04dc20036dbd8313ed055",
    "PhoneNumber": "+375333252224",
    "UserType": {
      "Name": "Driver"
    },
    "DateTimeLastAuth": "2021-05-17T00:00:00.000"
  }'
go