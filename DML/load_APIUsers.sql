use TrainingDB
go

exec dbo.usp_ParseJsonAPIUsers
     @json =N'{
      "Name": "Sergey API User",
      "Login": "sergey_api_user",
      "HashPassword": "81dc9bdb52d04dc20036dbd8313ed055",
      "PhoneNumber": "+375333252222",
      "DateTimeLastAuth": "2021-05-17T00:00:00.000"
    }'
go