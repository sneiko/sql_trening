use TrainingDB
go

exec dbo.usp_ParseJsonViewAnalytics
     @json = N'{
                      "Clients": [
                        {"PhoneNumber": "+375293361873"},
                        {"PhoneNumber": "+375333252011"}
                      ],
                      "Products": [
                        {"Name": "Coca-Cola 1l"},
                        {"Name": "Souse Teriaki"}
                      ],
                      "ShowNullRows": 1,
                      "ShowNullCols": 1
                  }'
go

exec dbo.usp_ParseJsonViewAnalytics
     @json = N'{
                      "Clients": [
                        {"PhoneNumber": "+375293361873"},
                        {"PhoneNumber": "+375333252011"}
                      ],
                      "Products": [
                        {"Name": "Coca-Cola 1l"},
                        {"Name": "Souse Teriaki"}
                      ],
                      "ShowNullRows": 0,
                      "ShowNullCols": 1
                  }'
go

exec dbo.usp_ParseJsonViewAnalytics
     @json = N'{
                      "Clients": [
                        {"PhoneNumber": "+375293361873"},
                        {"PhoneNumber": "+375333252011"}
                      ],
                      "Products": [
                        {"Name": "Coca-Cola 1l"},
                        {"Name": "Souse Teriaki"}
                      ],
                      "ShowNullRows": 0,
                      "ShowNullCols": 0
                  }'
go

exec dbo.usp_ParseJsonViewAnalytics
     @json = N'{
                      "Clients": [
                      ],
                      "Products": [
                        {"Name": "Coca-Cola 1l"},
                        {"Name": "Souse Teriaki"}
                      ],
                      "ShowNullRows": 0,
                      "ShowNullCols": 0
                  }'
go



exec dbo.usp_ParseJsonViewAnalytics
     @json = N'{
                      "Clients": [
                        {"PhoneNumber": "+375293361873"},
                        {"PhoneNumber": "+375333252011"}
                      ],
                      "Products": [
                      ],
                      "ShowNullRows": 0,
                      "ShowNullCols": 0
                  }'
go



exec dbo.usp_ParseJsonViewAnalytics
     @json = N'{
                      "Clients": [
                      ],
                      "Products": [
                      ],
                      "ShowNullRows": 0,
                      "ShowNullCols": 0
                  }'
go