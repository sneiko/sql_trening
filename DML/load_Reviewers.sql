use TrainingDB
go

exec dbo.usp_ParseJsonAddReviewers
     @json =N'{
        "Review": {
            "Rating": 4,
            "Description": "Не плохо, но может быть лучше"
        },
        "User": {
            "Login": "sergey_operator"
        },
        "Order": {
            "DeliveryAdress": "г.Минск, ул. Коммунистическая 10, кв 99",
            "DateTimeCreation": "2022-03-09 12:58:25.710"
        },
        "Client": {
            "PhoneNumber": "+375333252056"
        }
    }'
go