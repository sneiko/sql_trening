use TrainingDB
go

exec dbo.usp_ParseJsonMarketplaceStatuses
     @json =N'{
    "Name": "IsRead"
  }'
go

exec dbo.usp_ParseJsonMarketplaceStatuses
     @json =N'{
    "Name": "NotRead"
  }'
go