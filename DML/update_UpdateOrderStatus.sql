use TrainingDB
go

exec dbo.usp_ParseJsonUpdateOrderStatus
     @json =N'{
                "Order": {
                  "OrderId": 5,
                  "Status": "Cook"
                },
                "CookUser": {
                  "Login": "sergey_cook"
                }
              }'
go

exec dbo.usp_ParseJsonUpdateOrderStatus
     @json =N'{
                "Order": {
                  "OrderId": 5,
                  "Status": "InDelivery"
                },
                "DeliveryUser": {
                  "Login": "sergey_driver"
                }
              }'
go

exec dbo.usp_ParseJsonUpdateOrderStatusComplete
     @json =N'{
                "Order": {
                  "OrderId": 5,
                  "Status": "Complete"
                },
                "DeliveryUser": {
                  "Login": "sergey_driver"
                }
              }'
go