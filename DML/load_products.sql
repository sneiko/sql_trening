use TrainingDB
go

exec dbo.usp_ParseJsonAddProduct
     @json = N'{
  "Product": {
    "Name": "Product name",
    "Description": "Description",
    "CookTime": 15000,
    "AwailableForSale": 1,
    "ProductType": {
      "Name": "Pizza"
    }
  },
  "Ingredient": [
    {
      "Name": "Cheese",
      "Callories": 500,
      "Quantity": 4,
      "PricePerUnit": 94
    },
    {
      "Name": "Cucumber",
      "Callories": 400,
      "Quantity": 2,
      "PricePerUnit": 21
    },
    {
      "Name": "Dough",
      "Callories": 800,
      "Quantity": 8,
      "PricePerUnit": 45
    },
    {
      "Name": "Tomato",
      "Callories": 200,
      "Quantity": 1,
      "PricePerUnit": 11
    }
  ],
  "Administrator": {
    "Name": "sergey_administrator"
  }
}'
go


exec dbo.usp_ParseJsonAddProduct
     @json = N'{
  "Product": {
    "Name": "Coca-Cola 1l.",
    "Description": "Description",
    "CookTime": 15000,
    "AwailableForSale": 1,
    "ProductType": {
      "Name": "Pizza"
    }
  },
  "Ingredient": [
    {
      "Name": "Cheese",
      "Callories": 500,
      "Quantity": 4,
      "PricePerUnit": 94
    },
    {
      "Name": "Cucumber",
      "Callories": 400,
      "Quantity": 2,
      "PricePerUnit": 21
    },
    {
      "Name": "Dough",
      "Callories": 800,
      "Quantity": 8,
      "PricePerUnit": 45
    },
    {
      "Name": "Tomato",
      "Callories": 200,
      "Quantity": 1,
      "PricePerUnit": 11
    }
  ],
  "Administrator": {
    "Name": "sergey_administrator"
  }
}'
go


exec dbo.usp_ParseJsonAddProduct
     @json = N'{
  "Product": {
    "Name": "Souse Teriaki",
    "Description": "Description",
    "CookTime": 15000,
    "AwailableForSale": 1,
    "ProductType": {
      "Name": "Pizza"
    }
  },
  "Ingredient": [
    {
      "Name": "Teriaki",
      "Callories": 200,
      "Quantity": 1,
      "PricePerUnit": 11
    }
  ],
  "Administrator": {
    "Name": "sergey_administrator"
  }
}'
go