use TrainingDB
go

exec dbo.usp_ParseJsonMarketplace
     @json =N'{
      "Marketplace": {
        "Name": "Sergey Marketplace",
        "Description": "Marketplace description",
        "StatusName": "NotRead"
      },
      "APIUser": {
        "Login": "sergey_api_user"
      }
    }'
go